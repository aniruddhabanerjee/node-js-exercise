const express = require('express')
// For input validation
const Joi = require('joi')
//express is a function which returns an object
const app = express()

//added a piece of middleware
app.use(express.json())

const users = [
  { id: 1, name: 'Express User', email: 'test@gmail.com', age: 10 },
  { id: 2, name: 'Express User 2', email: 'test2@gmail.com', age: 10 },
  { id: 3, name: 'Express User 3', email: 'test3@gmail.com', age: 10 },
]
// app object has many useful methods like get, put, post, delete and many more
app.get('/', (req, res) => {
  res.send('Namaste JavaScript....')
})

app.get('/api/users', (req, res) => {
  res.send(users)
})

app.get('/api/users/:id', (req, res) => {
  const user = users.find((el) => el.id === +req.params.id)
  if (!user) {
    res.status(404).send('User Not Found')
    return
  }
})

app.post('/api/users', (req, res) => {
  const result = validateUser(req.body)

  if (result.error) {
    // 400 Bad Request
    return res.status(400).send(result.error.details[0].message)
  }
  // if (!req.body.name || !req.body.email) {
  //   // 400 Bad Request
  //   res.status(400).send('name and email both are required')
  //   return
  // }
  const user = {
    id: users.length + 1,
    name: req.body.name,
    email: req.body.email,
  }
  users.push(user)
  res.send(user)
})

app.put('/api/users/:id', (req, res) => {
  // Find the user
  // If not found return 404 not found
  const user = users.find((el) => el.id === +req.params.id)
  if (!user) {
    return res.status(404).send('User Not Found')
  }
  // validate changes
  // if invalid return 400 -Bad Request
  const result = validateUser(req.body)
  if (result.error) {
    // 400 Bad Request
    return res.status(400).send(result.error.details[0].message)
  }
  // Update course
  user.name = req.body.name
  user.email = req.body.email

  res.send(user)
})

app.delete('/api/users/:id', (req, res) => {
  const user = users.find((el) => el.id === +req.params.id)
  if (!user) {
    return res.status(404).send('User Not Found')
  }
  const index = users.indexOf(user)
  users.splice(index, 1)
  res.send(user)
})

const port = process.env.PORT || 3000
app.listen(port, () => console.log(`Listening on port ${port}....`))

function validateUser(user) {
  const schema = {
    name: Joi.string().min(3).required(),
    email: Joi.string().required(),
  }

  return Joi.validate(user, schema)
}
