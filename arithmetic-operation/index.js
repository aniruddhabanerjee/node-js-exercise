const arithmetic = {
  add(x, y) {
    return x + y
  },
  multiply(x, y) {
    return x * y
  },
  divide(x, y) {
    return x / y
  },
  subtract(x, y) {
    return x - y
  },
  modulo(x, y) {
    return x % y
  },
}
exports = arithmetic
