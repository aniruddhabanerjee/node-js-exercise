## Arithmetic Operators

The basic arithmetic operations for real numbers are addition, subtraction, multiplication, and division.

### Install

```
$ npm install arithmetic-operators
```

### Usage

```
var arithmetic = require('arithmetic-operators')

arithmetic.add(5,6) // => 11
```

### API

**arithmetic.add(num1, num2) -> number**<br>
_num1/num2_<br>
_Required_<br>
Type: number<br>

**arithmetic.subtract(num1, num2) -> number**<br>
_num1/num2_<br>
_Required_<br>
Type: number<br>

**arithmetic.multiply(num1, num2) -> number**<br>
_num1/num2_<br>
_Required_<br>
Type: number<br>

**arithmetic.divide(num1, num2) -> number**<br>
_num1/num2_<br>
_Required_<br>
Type: number<br>

**arithmetic.modulo(num1, num2) -> number**<br>
_num1/num2_<br>
_Required_<br>
Type: number<br>

Keywords

    add
    divide
    multiply
    modulo
    sum
