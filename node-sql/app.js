const faker = require('faker')
const mysql = require('mysql')

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'password',
  database: 'mail',
})

connection.connect()

//Selecting the data
// connection.query('SELECT 1+1 as solution', function (error, results, fields) {
//   if (error) throw error
//   console.log('The solution is: ', results)
// })

// connection.end()

// Insert data into the table
// for (let i = 0; i < 3; i++) {
//   connection.query(
//     `insert into users(email) values("${faker.internet.email()}")`,
//     function (error, results) {
//       if (error) throw error
//       console.log(results)
//     }
//   )
// }

const emails = []

for (let i = 0; i < 10; i++) {
  emails.push([faker.internet.email(), faker.date.past()])
}
console.log(emails)
const q = 'insert into users (email,created_at) values ?'

connection.query(q, [emails], function (error, results) {
  if (error) throw error
  console.log(results)
})

connection.end()
